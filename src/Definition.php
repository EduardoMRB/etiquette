<?php

namespace Etiquette;

class Definition
{
    private $paper = 'letter';
    private $knownPapers = ['letter'];
    private $dimensions = [215.9, 279.5];
    private $fontFace = 'Arial';
    private $fontSize = 12;
    private $topMargin = 0;
    private $rightMargin = 0;
    private $bottomMargin = 0;
    private $leftMargin = 0;
    private $labelHeight = 25.4;
    private $labelWidth = 66.7;
    private $textAlign = 'center';

    public function getPaper()
    {
        return $this->paper;
    }

    public function setPaper($paper)
    {
        if (!in_array($paper, $this->knownPapers)) {
            throw new \InvalidArgumentException('Invalid paper: ' . $paper);
        }
        $this->paper = $paper;
        return $this;
    }

    /**
     * Returns an array with the width and height respectively.
     * Ex: [215.9, 279.5]
     *
     * @return array
     */
    public function getDimensions()
    {
        return $this->dimensions;
    }

    /**
     * Sets the new dimensions, must be an array with two elements, first the width, then the height.
     *
     * @param array $dimensions
     * @return self
     */
    public function setDimensions(array $dimensions)
    {
        if ($this->validDimensions($dimensions)) {
            throw new \InvalidArgumentException('Invalid dimensions: ' . print_r($dimensions, true));
        }
        $this->dimensions = $dimensions;
        $this->paper = 'unkown';
        return $this;
    }

    private function validDimensions(array $dimensions)
    {
        $nonNumerics = array_filter($dimensions, function ($item) {
            return !is_numeric($item);
        });

        return count($dimensions) != 2 || count($nonNumerics) > 0;
    }

    public function getFontFace()
    {
        return $this->fontFace;
    }

    public function setFontFace($fontFace)
    {
        $this->fontFace = $fontFace;
        return $this;
    }

    public function getFontSize()
    {
        return $this->fontSize;
    }

    public function setFontSize($fontSize)
    {
        $this->fontSize = $fontSize;
        return $this;
    }

    public function setTopMargin($margin)
    {
        $this->validateMargin($margin);
        $this->topMargin = $margin;
        return $this;
    }

    public function getTopMargin()
    {
        return $this->topMargin;
    }

    public function setRightMargin($margin)
    {
        $this->validateMargin($margin);
        $this->rightMargin = $margin;
        return $this;
    }

    public function getRightMargin()
    {
        return $this->rightMargin;
    }

    public function getBottomMargin()
    {
        return $this->bottomMargin;
    }

    public function setBottomMargin($bottomMargin)
    {
        $this->validateMargin($bottomMargin);
        $this->bottomMargin = $bottomMargin;
        return $this;
    }

    public function getLeftMargin()
    {
        return $this->leftMargin;
    }

    public function setLeftMargin($leftMargin)
    {
        $this->validateMargin($leftMargin);
        $this->leftMargin = $leftMargin;
        return $this;
    }

    private function validateMargin($margin)
    {
        if (!is_numeric($margin)) {
            throw new \InvalidArgumentException(
                sprintf('Margin value must be numeric, %s given', print_r($margin, true))
            );
        }
    }

    public function getWidth()
    {
        list($width,) = $this->getDimensions();
        return $width;
    }

    public function getHeight()
    {
        list(, $height) = $this->getDimensions();
        return $height;
    }

    public function getLabelWidth()
    {
        return $this->labelWidth;
    }

    public function setLabelWidth($width)
    {
        $this->labelWidth = $width;
        return $this;
    }

    public function getLabelHeight()
    {
        return $this->labelHeight;
    }

    public function setLabelHeight($labelHeight)
    {
        $this->labelHeight = $labelHeight;
        return $this;
    }

    public function labelsPerRow()
    {
        return floor($this->getWidth() / $this->getLabelWidth());
    }

    public function rowsPerPage()
    {
        $verticalMargins = $this->getBottomMargin() + $this->getTopMargin();
        $heightAndMargin = $this->getHeight() - $verticalMargins;
        return floor($heightAndMargin / $this->getLabelHeight());
    }

    public function getTextAlign()
    {
        return $this->textAlign;
    }

    public function setTextAlign($textAlign)
    {
        $this->textAlign = $textAlign;
        return $this;
    }
}
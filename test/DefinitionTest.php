<?php


namespace Etiquette\Test;


use Etiquette\Definition;

class DefinitionTest extends \PHPUnit_Framework_TestCase
{
    public function test_instance()
    {
        $definition = new Definition();
        $this->assertInstanceOf('Etiquette\Definition', $definition);
    }

    public function test_default_paper_is_letter()
    {
        $definition = new Definition();
        $this->assertEquals('letter', $definition->getPaper());
    }

    public function test_we_can_set_available_types_of_papers()
    {
        $definition = new Definition();
        $definition->setPaper('letter');
        $this->assertEquals('letter', $definition->getPaper());
    }

    /** @expectedException \InvalidArgumentException */
    public function test_we_cannot_use_a_invalid_type_of_paper()
    {
        $definition = new Definition();
        $definition->setPaper('whatever-paper');
    }

    public function test_definition_has_default_dimension()
    {
        $definition = new Definition();
        $this->assertEquals([215.9, 279.5], $definition->getDimensions());
    }

    public function test_get_page_width_and_height()
    {
        $definition = new Definition();
        $this->assertEquals(215.9, $definition->getWidth());
        $this->assertEquals(279.5, $definition->getHeight());
    }

    public function test_we_can_set_dimensions()
    {
        $definition = new Definition();
        $definition->setDimensions([20.9, 39.8]);
        $this->assertEquals([20.9, 39.8], $definition->getDimensions());
    }

    public function invalidDimensions()
    {
        return [
            [[9, 292, 929, 2]],
            [[9]],
            [["a", "b"]],
            [[[], []]],
            [[8.98, "a"]],
            [["a", 8.98]],
        ];
    }

    /**
     * @expectedException \InvalidArgumentException
     * @dataProvider invalidDimensions
     * @param $invalidDimensions
     */
    public function test_dimensions_are_validated($invalidDimensions)
    {
        $definition = new Definition();
        $definition->setDimensions($invalidDimensions);
    }

    public function test_if_we_set_custom_dimensions_the_paper_is_unkown()
    {
        $definition = new Definition();
        $this->assertEquals('letter', $definition->getPaper());
        $definition->setDimensions([9, 8]);
        $this->assertEquals('unkown', $definition->getPaper());
    }

    public function test_default_font_is_arial()
    {
        $definition = new Definition();
        $this->assertEquals('Arial', $definition->getFontFace());
    }

    public function test_set_font()
    {
        $definition = new Definition();
        $definition->setFontFace('Helvetica');
        $this->assertEquals('Helvetica', $definition->getFontFace());
    }

    public function test_font_size()
    {
        $definition = new Definition();
        $this->assertEquals(12, $definition->getFontSize());
        $definition->setFontSize(14);
        $this->assertEquals(14, $definition->getFontSize());
    }

    public function test_margin_top()
    {
        $definition = new Definition();
        $definition->setTopMargin(30);
        $this->assertEquals(30, $definition->getTopMargin());
    }

    public function test_margin_right()
    {
        $definition = new Definition();
        $definition->setRightMargin(30);
        $this->assertEquals(30, $definition->getRightMargin());
    }

    public function test_margin_bottom()
    {
        $definition = new Definition();
        $definition->setBottomMargin(30);
        $this->assertEquals(30, $definition->getBottomMargin());
    }

    public function test_margin_left()
    {
        $definition = new Definition();
        $definition->setLeftMargin(30);
        $this->assertEquals(30, $definition->getLeftMargin());
    }

    public function invalidMarginsProvider()
    {
        return [
            ['setTopMargin', 'asdsa'],
            ['setRightMargin', 'asdsa'],
            ['setBottomMargin', 'asdsa'],
            ['setLeftMargin', 'asdsa'],
            ['setTopMargin', new \DateTime()],
            ['setRightMargin', new \DateTime()],
            ['setBottomMargin', 'substr'],
            ['setLeftMargin', function () { return 'callable'; }],
        ];
    }

    /**
     * @expectedException \InvalidArgumentException
     * @dataProvider invalidMarginsProvider
     * @param string $method method name to call on Definition
     * @param mixed $arg
     */
    public function test_we_cannot_set_an_invalid_margin($method, $arg)
    {
        $definition = new Definition();
        call_user_func([$definition, $method], $arg);
    }

    public function test_default_margins_are_zero()
    {
        $definition = new Definition();
        $this->assertEquals(0, $definition->getBottomMargin());
        $this->assertEquals(0, $definition->getLeftMargin());
        $this->assertEquals(0, $definition->getRightMargin());
        $this->assertEquals(0, $definition->getTopMargin());
    }

    public function test_label_size()
    {
        $definition = new Definition();
        $this->assertEquals(25.4, $definition->getLabelHeight());
        $this->assertEquals(66.7, $definition->getLabelWidth());

        $definition->setLabelWidth(30);
        $this->assertEquals(30, $definition->getLabelWidth());

        $definition->setLabelHeight(40);
        $this->assertEquals(40, $definition->getLabelHeight());
    }

    public function test_default_label_per_row()
    {
        $definition = new Definition();
        $this->assertEquals(3, $definition->labelsPerRow());
    }

    public function test_rows_per_page()
    {
        $definition = new Definition();
        $definition->setTopMargin(12)
            ->setBottomMargin(0)
            ->setLeftMargin(7.9)
            ->setRightMargin(7.9);
        $this->assertEquals(10, $definition->rowsPerPage());
    }

    public function test_paper_with_two_labels()
    {
        $definition = new Definition();
        $definition->setDimensions([200, 1000])
            ->setLabelWidth(100)
            ->setLabelHeight(1000);

        $this->assertEquals(1, $definition->rowsPerPage());
        $this->assertEquals(2, $definition->labelsPerRow());
    }

    public function test_we_can_set_text_alignment()
    {
        $definition = new Definition();
        $this->assertEquals('center', $definition->getTextAlign());
        $definition->setTextAlign('justify');
        $this->assertEquals('justify', $definition->getTextAlign());
    }
}
